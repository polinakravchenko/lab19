﻿// lab19_1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <cmath>

#define _USE_MATH_DEFINES
#include <math.h>


struct Shape2D {
    virtual double perimeter() const = 0;
    virtual double area() const = 0;
    virtual ~Shape2D() = default;
};

class Rectangle2D : public Shape2D {
private:
    int x;
    int y;
    int width;
    int height;
public:
    Rectangle2D(int x, int y, int width, int height)
        : x(x), y(y), width(width), height(height) {}

    virtual ~Rectangle2D() = default;

    double perimeter() const override {
        return 2 * (width + height);
    }

    double area() const override {
        return width * height;
    }

    static Rectangle2D of(int x1, int y1, int x2, int y2)
    {
        return Rectangle2D(x1, y1, std::abs(x2 - x1), std::abs(y2 - y1));
    }
};

class Circle2D : public Shape2D {
private:
    int x;
    int y;
    int R;
public:
    Circle2D(int x, int y, int r) : x(x), y(y), R(r) {}

    virtual ~Circle2D() = default;

    double perimeter() const override {
        return 2 * M_PI * R;
    }

    double area() const override {
        return M_PI * R * R;
    }
};

class Ellipse2D : public Shape2D {
private:
    int x;
    int y;
    int R1;
    int R2;
public:

    Ellipse2D(int x, int y, int r1, int r2) : x(x), y(y), R1(r1), R2(r2) {}

    virtual ~Ellipse2D() = default;

    double perimeter() const override {
        return 2 * M_PI * sqrt(0.5 * (pow(R1, 2) + pow(R2, 2)));
    }

    double area() const override {
        return M_PI * R1 * R2;
    }

};

class Square2D : public Shape2D {
private:
    int x;
    int y;
    int width;
    int height;
public:
    Square2D(int x, int y, int width, int height)
        : x(x), y(y), width(width), height(height) {}

    virtual ~Square2D() = default;

    double perimeter() const override {
        return 2 * (width + height);
    }

    double area() const override {
        return width * height;
    }

    static Square2D of(int x1, int y1, int x2, int y2)
    {
        return Square2D(x1, y1, std::abs(x2 - x1), std::abs(y2 - y1));
    }
};

class Triangle2D : public Shape2D {
private:
    int b;
    int h;
    int a;
    int c;
public:
    Triangle2D(int b, int h, int a, int c)
        : b(b), h(h), a(a), c(c) {}

    virtual ~Triangle2D() = default;

    double perimeter() const override {
        return a + b + c;
    }

    double area() const override {
        return (b * h) / 2;
    }
};

class EqTriangle2D : public Shape2D {
private:
    int a;
    int h;
public:
    EqTriangle2D(int a, int h)
        : a(a), h(h) {}

    virtual ~EqTriangle2D() = default;

    double perimeter() const override {
        return 3 * a;
    }

    double area() const override {
        return pow(h, 2) / sqrt(3);
    }
};

class ScTriangle2D : public Shape2D {
private:
    int a;
    int b;
    int c;
    int s;
public:
    ScTriangle2D(int a, int b, int c, int s)
        : a(a), b(b), c(c), s(s) {}

    virtual ~ScTriangle2D() = default;

    double perimeter() const override {
        return a + b + c;
    }
    double area() const override {
        return sqrt(s * (s - a) * (s - b) * (s - c));
    }
};

int main() {
    Rectangle2D r1(100, 200, 50, 40);
    Circle2D c1(-10, 7, 10);
    Ellipse2D e1(-5, 6, 15, 20);
    Square2D sq1(50, 50, 50, 50);
    Triangle2D t1(50, 15, 20, 25);
    EqTriangle2D t2(35, 20);
    ScTriangle2D t3(100, 85, 60, 122.5);
    const Shape2D* s1 = &r1;
    const Shape2D& s2 = Circle2D(-10, 7, 10);
    const Shape2D& s3 = Ellipse2D(-5, 6, 15, 20);
    const Shape2D& s4 = Square2D(50, 50, 50, 50);
    const Shape2D* s5 = &t1;
    const Shape2D* s6 = &t2;
    const Shape2D* s7 = &t3;
    std::cout << "Rectangle" << std::endl;
    std::cout << s1->area() << ": " << s1->perimeter() << std::endl;
    std::cout << "============" << std::endl;
    std::cout << "Circle" << std::endl;
    std::cout << s2.area() << ": " << s2.perimeter() << std::endl;
    std::cout << "============" << std::endl;
    std::cout << "Ellipse" << std::endl;
    std::cout << s3.area() << ": " << s3.perimeter() << std::endl;
    std::cout << "============" << std::endl;
    std::cout << "Square" << std::endl;
    std::cout << s4.area() << ": " << s4.perimeter() << std::endl;
    std::cout << "============" << std::endl;
    std::cout << "Triangle" << std::endl;
    std::cout << s5->area() << ": " << s5->perimeter() << std::endl;
    std::cout << "============" << std::endl;
    std::cout << "Equilateral Triangle" << std::endl;
    std::cout << s6->area() << ": " << s6->perimeter() << std::endl;
    std::cout << "============" << std::endl;
    std::cout << "Scalene Triangle" << std::endl;
    std::cout << s7->area() << ": " << s7->perimeter() << std::endl;
    std::cout << "============" << std::endl;
    return 0;
}
